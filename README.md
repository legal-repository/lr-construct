# Legal Repository Construction Website

This is the official construction website for the Legal Repository. The HTML and CSS available on the website are made available under the MIT license. However, the images and logos are protected by the copyright and trademark of the Legal Repository. They may not be used without seeking relevant authorization. Usage without permission would be considered infringement and shall be dealt to the maximum extent of the law.
